<?php
declare(strict_types=1);

namespace App\Validators;

use App\Validators\Constraints\UserRegisterConstraints;
use Symfony\Component\Validator\Validation;

final class PostDataCheck
{
    private $validation;
    /**
     * @var UserRegisterConstraints
     */
    private $userRegisterConstraints;

    public function __construct(UserRegisterConstraints $userRegisterConstraints)
    {
        $this->validation = Validation::createValidator();
        $this->userRegisterConstraints = $userRegisterConstraints;
    }

    public function dataValidation($data)
    {
        return $this->validation->validate(
            $data,
            $this->userRegisterConstraints->getConstraints()
        );
    }
}
