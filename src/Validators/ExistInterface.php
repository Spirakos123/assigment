<?php
declare(strict_types=1);

namespace App\Validators;

interface ExistInterface
{
    public function checkIfExists(string $data): bool;
}