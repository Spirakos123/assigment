<?php
declare(strict_types=1);

namespace App\Validators\Violations;

final class PostDataCheckViolations
{
    public function checkErrors($violations)
    {
        if ($violations->count() > 0) {
            $errors = [];
            foreach ($violations as $key => $error) {
                $errors[$key]['field'] = $error->getPropertyPath();
                $errors[$key]['error_message'] = $error->getMessage();
            }
            throw new FieldsErrorException('error',$errors);
        }
    }
}
