<?php
declare(strict_types=1);

namespace App\Validators\Violations;

use Exception;

class FieldsErrorException extends Exception
{
    private $_data = '';

    public function __construct(string $message, array $data)
    {
        $this->_data = $data;
        parent::__construct($message);
    }

    public function getDataErrors()
    {
        return $this->_data;
    }
}