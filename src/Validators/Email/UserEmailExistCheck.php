<?php
declare(strict_types=1);

namespace App\Validators\Email;

use App\Entity\Users;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

final class UserEmailExistCheck extends AbstractController
{
    public function checkIfExists(string $email)
    {
        $emailExists = $this->getDoctrine()
            ->getRepository(Users::class)
            ->findOneBy(['email' => $email]);

        if (!empty($emailExists)) {
            throw new EmailExistsException();
        }

        return false;
    }
}
