<?php
declare(strict_types=1);

namespace App\Validators\Constraints;

use Symfony\Component\Validator\Constraints as Assert;

final class UserRegisterConstraints
{
    /**
     * @var Assert\Collection
     */
    private $constraint;

    public function __construct()
    {
        $this->constraint = new Assert\Collection([
            'name' => new Assert\NotBlank(),
            'surname' => new Assert\NotBlank(),
            'password' => new Assert\NotBlank(),
            'email' => new Assert\Email(),
        ]);
    }

    public function getConstraints()
    {
        return $this->constraint;
    }
}
