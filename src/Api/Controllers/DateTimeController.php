<?php

namespace App\Api\Controllers;

use DateTimeImmutable;
use DateTimeZone;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class DateTimeController extends AbstractController
{
    /**
     * @Route("/get-date", name="currentDate")
     */
    public function getCurrentDate()
    {
        $response = new JsonResponse();

        $currentDate = new DateTimeImmutable();

        $currentDate = $currentDate->setTimezone(new DateTimeZone('Europe/Athens'));

        $currentDate = $currentDate->format('d/m/Y H:i');

        $response->setData(['data' => $currentDate]);

        return $response;
    }
}