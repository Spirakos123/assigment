<?php
declare(strict_types=1);

namespace App\Api\Controllers\Cache;


interface CacheDataInterface
{
    public function dataSave(string $data);

    public function dataRetrieve(string $key): string;
}