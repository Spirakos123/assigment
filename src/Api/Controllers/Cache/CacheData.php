<?php
declare(strict_types=1);

namespace App\Api\Controllers\Cache;

use App\Api\Controllers\Auth\Service\AccessTokenCallApiInterface;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Contracts\Cache\ItemInterface;

final class CacheData implements CacheDataInterface
{
    /**
     * @var FilesystemAdapter
     */
    private $cache;
    /**
     * @var AccessTokenCallApiInterface
     */
    private $accessToken;

    public function __construct(AccessTokenCallApiInterface $accessToken)
    {
        $this->cache = new FilesystemAdapter();
        $this->accessToken = $accessToken;
    }

    public function dataSave(string $data)
    {
        $this->deleteItem('token');
        try {
            $this->cache->get('token', function (ItemInterface $item) use ($data) {
                $item->expiresAfter(300);

                return $data;
            });
        } catch (InvalidArgumentException $e) {
        }
    }

    public function dataRetrieve(string $key): string
    {
        try {
            $data = $this->cache->get($key, function (ItemInterface $item) {
                $item->expiresAfter(300);
                return $this->accessToken->callApi();
            });
        } catch (InvalidArgumentException $e) {
        }

        if (empty($data)) {
            return '';
        }

        return $data;
    }

    private function deleteItem(string $key)
    {
        try {
            $this->cache->delete($key);
        } catch (InvalidArgumentException $e) {
        }
    }
}
