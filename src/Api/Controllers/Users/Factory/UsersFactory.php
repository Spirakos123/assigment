<?php
declare(strict_types=1);

namespace App\Api\Controllers\Users\Factory;

//use App\Api\Controllers\Users\Entity\UsersEntity;
use App\Entity\Users;

final class UsersFactory
{
    public function setUserOjbWithAttributes(array $users): array
    {
        foreach ($users as $user) {
            $output[] = $this->serializeFields($user);
        }

        return $output;
    }

    private function serializeFields(Users $user)
    {
        return [
            'name' => $user->getName(),
            'surname' => $user->getSurname(),
            'email' => $user->getEmail(),
        ];

    }
}