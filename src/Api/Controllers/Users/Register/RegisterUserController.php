<?php
declare(strict_types=1);

namespace App\Api\Controllers\Users\Register;

use App\Entity\Users;
use App\Validators\Email\EmailExistsException;
use App\Validators\Email\UserEmailExistCheck;
use App\Validators\PostDataCheck;
use App\Validators\Violations\FieldsErrorException;
use App\Validators\Violations\PostDataCheckViolations;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegisterUserController extends AbstractController
{
    /**
     * @var PostDataCheck
     */
    private $postDataCheck;
    /**
     * @var PostDataCheckViolations
     */
    private $postDataCheckViolations;
    /**
     * @var UserEmailExistCheck
     */
    private $userEmailExistCheck;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(
        PostDataCheck $postDataCheck,
        PostDataCheckViolations $postDataCheckViolations,
        UserEmailExistCheck $userEmailExistCheck,
        UserPasswordEncoderInterface $encoder
    ) {
        $this->postDataCheck = $postDataCheck;
        $this->postDataCheckViolations = $postDataCheckViolations;
        $this->userEmailExistCheck = $userEmailExistCheck;
        $this->encoder = $encoder;
    }

    /**
     * @Route("user/register", methods={"POST"})
     */
    public function registerUser(Request $request): JsonResponse
    {
        $data = json_decode(
            $request->getContent(),
            true
        );
        $violations = $this->postDataCheck->dataValidation($data['body']);
        try {
            $this->postDataCheckViolations->checkErrors($violations);
        } catch (FieldsErrorException $exception) {
            return new JsonResponse([
                "error" => $exception->getDataErrors()
            ]);
        }

        $data = $data['body'];

        $name = $data['name'];
        $surname = $data['surname'];
        $password = $data['password'];
        $email = $data['email'];

        try {
            $this->userEmailExistCheck->checkIfExists($email);
        } catch (EmailExistsException $exception) {
            return new JsonResponse([
                "error" => 'Email already exists.'
            ], 500);
        }

        $entityManager = $this->getDoctrine()->getManager();

        $user = new Users();

        $encoded = $this->encoder->encodePassword($user,$password);

        $user->setName($name)
        ->setSurname($surname)
        ->setPlainPassword($password)
        ->setPassword($encoded)
        ->setEmail($email);



        $entityManager->persist($user);

        try {
            $entityManager->flush();
        } catch (Exception $e) {
            return new JsonResponse(["error" => $e->getMessage()], 500);
        }

        return new JsonResponse([
            "msg" => 'Success'
        ], 200);
    }
}