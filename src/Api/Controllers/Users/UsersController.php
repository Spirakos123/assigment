<?php
declare(strict_types=1);

namespace App\Api\Controllers\Users;

use App\Api\Controllers\Auth\Service\CurlErrorException;
use App\Api\Controllers\Auth\SessionsDataInterface;
use App\Api\Controllers\Cache\CacheDataInterface;
use App\Api\Controllers\Users\Factory\UsersFactory;
use App\Entity\Users;
use App\Helpers\HTTP\RequestHeadersAuthorizationCheckInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class UsersController extends AbstractController
{
    /**
     * @var UsersFactory
     */
    private $usersFactory;
    /**
     * @var SessionInterface
     */
    private $session;
    /**
     * @var CacheDataInterface
     */
    private $cache;
    /**
     * @var RequestHeadersAuthorizationCheckInterface
     */
    private $authorizationHeaderCheck;

    public function __construct(
        UsersFactory $usersFactory,
        SessionsDataInterface $session,
        CacheDataInterface $cache,
        RequestHeadersAuthorizationCheckInterface $authorizationHeaderCheck
    ) {
        $this->usersFactory = $usersFactory;
        $this->session = $session;
        $this->cache = $cache;
        $this->authorizationHeaderCheck = $authorizationHeaderCheck;
    }

    /**
     * @Route("/get-users", name="userData")
     * @param Request $request
     * @return JsonResponse
     */
    public function getUsers(Request $request)
    {
        $response = new JsonResponse();

        if (
            empty($bearer = $this->authorizationHeaderCheck->authorizationCheck($request))
        ) {
            $response->setData('Unauthorize');
            $response->setStatusCode(401);
            return $response;
        }

        //$bearer = $request->headers->get('Authorization');
        if (preg_match("@Bearer\s(\S+)@",$bearer,$matches)) {
            if (!empty($matches)) {
                $bearer = $matches[1];
            }
        }
//        $token = $this->session->tokenRetrieve('token');
        try {
            $token = $this->cache->dataRetrieve('token');
        } catch (CurlErrorException $exception) {
            return new JsonResponse($exception->getMessage());
        }
//        $token = $this->cache->dataRetrieve('token');

        if (!$bearer) {
            $response->setData('Unauthorized');
            $response->setStatusCode(401);
            return $response;
        }else if ($bearer !== $token) {
            $response->setData('Unauthorized');
            $response->setStatusCode(401);
            return $response;
        }


        $users = $this->getDoctrine()
            ->getRepository(Users::class)
            ->findAll();

        if (!$users) {
            $response->setData(['users' => []]);
            return $response;
        }

        $usersSerialized = $this->usersFactory->setUserOjbWithAttributes($users);

        $response->setData(['users' => $usersSerialized]);

        return $response;
    }

}
