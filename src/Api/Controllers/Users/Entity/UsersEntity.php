<?php
declare(strict_types=1);

namespace App\Api\Controllers\Users\Entity;

final class UsersEntity
{
    /**
     * @var
     */
    private $username;
    /**
     * @var
     */
    private $surname;
    /**
     * @var
     */
    private $email;

    public function __construct(
      string $username,
      string $surname,
      string $email
    ) {
        $this->username = $username;
        $this->surname = $surname;
        $this->email = $email;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function getEmail()
    {
        return $this->email;
    }

}
