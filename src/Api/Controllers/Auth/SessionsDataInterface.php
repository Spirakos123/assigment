<?php
declare(strict_types=1);
namespace App\Api\Controllers\Auth;

interface SessionsDataInterface
{
    public function tokenSave(string $token);

    public function tokenRetrieve(string $key);
}