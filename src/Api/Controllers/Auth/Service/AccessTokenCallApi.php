<?php
declare(strict_types=1);

namespace App\Api\Controllers\Auth\Service;

final class AccessTokenCallApi implements AccessTokenCallApiInterface
{
    public function callApi(): string
    {
        $curl = curl_init();

        $postFields = [
            "client_id"     => "41dbOm8Ra1J5eOZcf2oTQjMX6Pj3UHmi",
            "client_secret" => "2oBkcRjy0jQf5Vxq7tszLqYVfZ_lY1Zd0UQoYby3Seb4By0dy7fwNH99vR989Rxa",
            "audience"      => "https://dev-c9iiltgb.eu.auth0.com/api/v2/",
            "grant_type"    => "client_credentials"
        ];
        $postFields = json_encode($postFields, JSON_FORCE_OBJECT);

        $postHeaders = [
            "content-type: application/json"
        ];

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://dev-c9iiltgb.eu.auth0.com/oauth/token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $postFields,
            CURLOPT_HTTPHEADER => $postHeaders,
        ));

        $response = curl_exec($curl);
        $error = curl_error($curl);

        curl_close($curl);

        if (!empty($error)) {
            throw new CurlErrorException;
        }

        $response = json_decode($response);

        $token = $response->access_token;

        return $token;
    }
}
