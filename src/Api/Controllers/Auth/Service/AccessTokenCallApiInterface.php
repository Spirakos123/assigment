<?php
declare(strict_types=1);

namespace App\Api\Controllers\Auth\Service;

interface AccessTokenCallApiInterface
{
    public function callApi():string ;
}