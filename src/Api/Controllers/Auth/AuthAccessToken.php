<?php
declare(strict_types=1);

namespace App\Api\Controllers\Auth;

//use Symfony\Component\HttpClient\HttpClient;
use App\Api\Controllers\Auth\Service\AccessTokenCallApiInterface;
use App\Api\Controllers\Auth\Service\CurlErrorException;
use App\Api\Controllers\Cache\CacheDataInterface;
use GuzzleHttp\Psr7\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

final class AuthAccessToken extends AbstractController
{
//    private $httpClient;
//    /**
//     * @var SessionInterface
//     */
//    private $session;
//    /**
//     * @var SessionsDataInterface
//     */
//    private $sessionsData;
    /**
     * @var CacheDataInterface
     */
    private $cache;
    /**
     * @var AccessTokenCallApiInterface
     */
    private $accessToken;

    public function __construct(
        SessionsDataInterface $session,
        CacheDataInterface $cache,
        AccessTokenCallApiInterface $accessToken
//        HttpClientInterface $httpClient
    ) {
        $this->session = $session;
        $this->cache = $cache;
//        $this->httpClient = $httpClient;
        $this->accessToken = $accessToken;
    }

    /**
     * @Route ("/token", methods={"POST"})
     */
    public function getToken()
    {
//        $session = new Session();
//        $headers = ['content-type' => 'application/json'];
//        $params = [
//            'body' => [
//                'client_id' => '41dbOm8Ra1J5eOZcf2oTQjMX6Pj3UHmi',
//                'client_secret' => '2oBkcRjy0jQf5Vxq7tszLqYVfZ_lY1Zd0UQoYby3Seb4By0dy7fwNH99vR989Rxa',
//                'audience' => 'https://dev-c9iiltgb.eu.auth0.com/api/v2/',
//                'grant_type' => 'client_credentials'
//            ]
////            'headers' => [
////                'content-type' => 'application/json'
////            ]
//        ];
//        $json = json_encode($params);
//        $client = new \GuzzleHttp\Client();
//        $res = $client->post(
//            "https://dev-c9iiltgb.eu.auth0.com/oauth/token",
//            [$params]
//        );
//        $a = '';
//        $response = $client->post('https://dev-c9iiltgb.eu.auth0.com/oauth/token',
//            [
//                $headers,
//                $json
//            ]);
////        $response = $client = new Request(
////            'POST',
////            'https://dev-c9iiltgb.eu.auth0.com/oauth/token',
////            $headers,
////            $json
////        );
//        $body = $response->getBody()->getContents();

        try {
            $token = $this->accessToken->callApi();
        } catch (CurlErrorException $exception) {
            return new JsonResponse($exception->getMessage());
        }

        $this->cache->dataSave($token);
//            $this->session->tokenSave($token);
//            $session->set('token', $token);
//            $this->session->getFlashBag()->add('notice', 'Set token');
        return new JsonResponse($token);


    }
}
