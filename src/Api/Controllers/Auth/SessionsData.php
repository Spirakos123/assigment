<?php
declare(strict_types=1);


namespace App\Api\Controllers\Auth;


use Symfony\Component\HttpFoundation\Session\SessionInterface;

final class SessionsData implements SessionsDataInterface
{
    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function tokenSave(string $token)
    {
        $this->session->set('token', $token);
    }

    public function tokenRetrieve(string $key)
    {
        return $this->session->get($key);
    }

}
