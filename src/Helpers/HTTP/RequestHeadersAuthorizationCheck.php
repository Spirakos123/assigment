<?php
declare(strict_types=1);


namespace App\Helpers\HTTP;

use Symfony\Component\HttpFoundation\Request;

final class RequestHeadersAuthorizationCheck implements RequestHeadersAuthorizationCheckInterface
{
    public function authorizationCheck(Request $request): string
    {
        $authorization = $request->headers->get('Authorization');
        if (empty($authorization)){
            return '';
        }

        return $authorization;
    }
}
