<?php
declare(strict_types=1);

namespace App\Helpers\HTTP;

use Symfony\Component\HttpFoundation\Request;

interface RequestHeadersAuthorizationCheckInterface
{
    public function authorizationCheck(Request $request): string;
}